# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

HayDay is an Android game, free to play from the Google Play store. This bot mines corn and sells it in the shop.

* Version

None at present

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Install AutoIT v3 (with SciTE editor) and BlueStacks under windows

* Configuration

Download and start game in emulator under windows.
Start haydaybot. To configure click to turn on "Set Coords" mode and press the 1, 2, 3 4 keys when the mouse is positioned
correctly in the game window. Click the game window anywhere first to set the active window and zoom so that
the plot and the store are both on the screen.

Only works with rectangular plots.

* 1 = Top left corner

* 2 = Bottom corner

* 3 = Right corner

* 4 = Red flowers next to store

![plot_config.jpg](https://bitbucket.org/repo/dAx9Ex/images/1588978772-plot_config.jpg)

Turn off "SetCoords" mode.
Configure plot size (columns run along the road, rows run away from road) and shop size (up to 2 x 5)
Configure existing corn already for sale, silo count and silo max
OK and start automation

* Dependencies

AutoIt v3 and BlueStacks Android emulation

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Email me at ptrrssll@gmail.com

* Other community or team contact