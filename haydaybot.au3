#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=haydaybot.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;
; HayDay Bot Player
; Designed for use on the BlueStacks Android Emulator - http://www.bluestacks.com/
;
; Copyright (c) 2014 Peter Russell
;

#include <MsgBoxConstants.au3>

;
; KODA Generated
;

#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#Region ### START Koda GUI section ### Form=F:\Peter\My Autoit\HayDayBot\BotConfig.kxf
$BotConfig = GUICreate("BotConfig", 240, 595, 192, 124)
$setCoords = GUICtrlCreateCheckbox("Set Coords", 8, 96, 81, 25)
$Label_CM = GUICtrlCreateLabel("Mouse", 8, 64, 33, 17)
$Label_X = GUICtrlCreateLabel("X", 128, 48, 11, 17)
$Label_Y = GUICtrlCreateLabel("Y", 184, 48, 11, 17)
$colour = GUICtrlCreateInput("", 56, 64, 49, 21)
$mouseX = GUICtrlCreateInput("", 112, 64, 49, 21)
$mouseY = GUICtrlCreateInput("", 168, 64, 49, 21)
$Label1 = GUICtrlCreateLabel("Plot Corner 1", 8, 120, 80, 17)
$Label2 = GUICtrlCreateLabel("Plot Corner 2", 8, 144, 80, 17)
$Label3 = GUICtrlCreateLabel("Plot Corner 3", 8, 168, 80, 17)
$plot1X = GUICtrlCreateInput("", 112, 120, 49, 21)
$plot2X = GUICtrlCreateInput("", 112, 144, 49, 21)
$plot3X = GUICtrlCreateInput("", 112, 168, 49, 21)
$plot1Y = GUICtrlCreateInput("", 168, 120, 49, 21)
$plot2Y = GUICtrlCreateInput("", 168, 144, 49, 21)
$plot3Y = GUICtrlCreateInput("", 168, 168, 49, 21)
$Label4 = GUICtrlCreateLabel("Rows", 112, 224, 31, 17)
$Label5 = GUICtrlCreateLabel("Columns", 168, 224, 44, 17)
$plotRows = GUICtrlCreateInput("", 112, 240, 49, 21)
$plotColumns = GUICtrlCreateInput("", 168, 240, 49, 21)
$bPlotTest = GUICtrlCreateButton("Plot Test", 8, 504, 65, 25)
$bPlantCorn = GUICtrlCreateButton("Plant Corn", 8, 528, 65, 25)
$bHarvest = GUICtrlCreateButton("Harvest", 8, 552, 65, 25)
$bOK = GUICtrlCreateButton("OK", 168, 336, 49, 25)
$Label6 = GUICtrlCreateLabel("Colour", 64, 48, 35, 17)
$shopX = GUICtrlCreateInput("", 112, 192, 49, 21)
$shopY = GUICtrlCreateInput("", 168, 192, 49, 21)
$Label7 = GUICtrlCreateLabel("Shop Activate 4", 8, 192, 80, 17)
$Label10 = GUICtrlCreateLabel("Plot", 72, 240, 25, 17)
$Label11 = GUICtrlCreateLabel("Shop", 72, 264, 32, 17)
$shopRows = GUICtrlCreateInput("", 112, 264, 49, 21)
$shopColumns = GUICtrlCreateInput("", 168, 264, 49, 21)
$bShopTest = GUICtrlCreateButton("Shop Test", 88, 504, 65, 25)
$bpStart = GUICtrlCreateButton("Start", 8, 400, 65, 25)
$bpStop = GUICtrlCreateButton("Stop", 8, 424, 65, 25)
$Label8 = GUICtrlCreateLabel("Automation", 8, 368, 94, 17)
$Label9 = GUICtrlCreateLabel("Testing", 8, 488, 39, 17)
$Label12 = GUICtrlCreateLabel("HayDay Bot", 8, 8, 150, 36)
GUICtrlSetFont(-1, 20, 400, 0, "Arial")
$plot_txt = GUICtrlCreateLabel("XXXXXXXXX", 8, 456, 64, 22)
$Label13 = GUICtrlCreateLabel("Plot", 8, 384, 22, 17)
$Label15 = GUICtrlCreateLabel("Corn", 24, 304, 26, 17)
$cornAmt = GUICtrlCreateInput("", 112, 304, 49, 21)
$Label16 = GUICtrlCreateLabel("Silo", 112, 288, 22, 17)
$cornMax = GUICtrlCreateInput("", 168, 304, 49, 21)
$Label17 = GUICtrlCreateLabel("Max", 168, 288, 24, 17)
$Label14 = GUICtrlCreateLabel("ForSale", 56, 288, 40, 17)
$forSale = GUICtrlCreateInput("", 56, 304, 49, 21)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

;
; INITIALIZE
;

; Hot Keys
Dim $data_lock = 0

; Auto state
; 0 = stopped, 1 = harvest, 2 = plant, 3 = shop, 4 = set 5mins, 5 = wait
Dim $plot_state = 0
Dim $plot_timer

; Scale
Dim $x_scale
Dim $y_scale
Dim $speed = 10

; Passoff global position
Dim $hWnd ; client window handle
Dim $gx
Dim $gy

Dim $gshopX
Dim $gshopY

; Stndard colours
Dim $greyC  = 0xF1F1F1
Dim $whiteC = 0xFFFFFF

; Init GUI with useful defaults

GUICtrlSetData($plotRows, 9)
GUICtrlSetData($plotColumns, 5)
GUICtrlSetData($shopRows, 2)
GUICtrlSetData($shopColumns, 5)
GUICtrlSetData($cornMax, 275)
GUICtrlSetData($forSale, 0)

GUICtrlSetState($bpStart, $GUI_DISABLE)
GUICtrlSetState($bpStop, $GUI_DISABLE)
GUICtrlSetData($plot_txt, "")

GUICtrlSetState($bPlotTest, $GUI_DISABLE)

;
; Start program
;

Opt("MouseCoordMode", 2) ;1=absolute, 0=relative, 2=client
Opt("PixelCoordMode", 2) ;1=absolute, 0=relative, 2=client

main()
exit

;
; Main processing loop
;

Func main()
	Local $x, $y, $c, $aPos, $t

	$hWnd = WinActivate("[REGEXPTITLE:BlueStacks.*]")
	If ($hWnd = 0) Then
		Msgbox(0, "Error", "WinActivate: Failed to find BlueStacks window")
		return
	EndIf

	While 1
		; display mouse pos and pixel colour

		$aPos = MouseGetPos()
		$x = $aPos[0]
		$y = $aPos[1]
		$c = PixelGetColor ($x, $y, $hWnd)
		GUICtrlSetData($mouseX, $x)
		GUICtrlSetData($mouseY, $y)
		GUICtrlSetData($colour, Hex($c, 6))

		; switch on windows msg type
		Switch GuiGetMsg()
		Case $GUI_EVENT_CLOSE
			ExitLoop

		Case $plotRows
			if GUICtrlRead($plotRows) = 0 Then
				GUICtrlSetData($plotRows, "")
			EndIf

		Case $plotColumns
			if GUICtrlRead($plotColumns) = 0 Then
				GUICtrlSetData($plotColumns, "")
			EndIf

		Case $bPlotTest
			click_plot(0, 0)

		Case $bShopTest
			shop_auto()

		Case $bPlantCorn
			plant_corn()

		Case $bHarvest
			harvest()

		Case $bOK
			config_plot()

		Case $bpStart
			plot_start()

		Case $bpStop
			plot_stop()

		Case $setCoords
			If _IsChecked($setCoords) then
				HotKeySet("1", "data_lock1")
				HotKeySet("2", "data_lock2")
				HotKeySet("3", "data_lock3")
				HotKeySet("4", "data_lock4")
			Else
				HotKeySet("1")
				HotKeySet("2")
				HotKeySet("3")
				HotKeySet("4")
				GUICtrlSetData($mouseX, "")
				GUICtrlSetData($mouseY, "")
			EndIf

		Case Else
			If _IsChecked($setCoords) then
				Switch $data_lock
				case 1
					GUICtrlSetData($plot1X, $aPos[0])
					GUICtrlSetData($plot1Y, $aPos[1])
					$data_lock = 0
				case 2
					GUICtrlSetData($plot2X, $aPos[0])
					GUICtrlSetData($plot2Y, $aPos[1])
					$data_lock = 0
				case 3
					GUICtrlSetData($plot3X, $aPos[0])
					GUICtrlSetData($plot3Y, $aPos[1])
					$data_lock = 0
				case 4
					GUICtrlSetData($shopX, $aPos[0])
					GUICtrlSetData($shopY, $aPos[1])
					$data_lock = 0
				EndSwitch
			EndIf

			If ($plot_state > 0) Then
				$t = TimerDiff($plot_timer)
				If ($t > 10000) Then
					plot_auto() ; check auto state
					$plot_timer = TimerInit()
				Else
					GUICtrlSetData($plot_txt, "Wait " & 10 - int($t / 1000))
				EndIf
			EndIf
		EndSwitch
	WEnd
EndFunc

;
; Automatic Plot Daemon
;

Func plot_start()
	GUICtrlSetState($bpStart, $GUI_DISABLE)
	GUICtrlSetState($bpStop, $GUI_ENABLE)
	; start timer set initial state
	$plot_timer = TimerInit()
	$plot_state = 1
EndFunc

Func plot_auto()
	Switch $plot_state
		Case 1
			GUICtrlSetData($plot_txt, "Harvest")
			If harvest() Then
				sleep(1000)
				GUICtrlSetData($plot_txt, "Plant")
				If plant_corn() Then
					$plot_state = 3
				Else
					$plot_state = 2
				EndIf
			Else
				$plot_state = 3
			EndIf

		Case 2
			GUICtrlSetData($plot_txt, "Plant")
			If plant_corn() Then
				$plot_state = 1
			EndIf

		Case 3
			GUICtrlSetData($plot_txt, "Shop")
			shop_auto()
			$plot_state = 1
	EndSwitch
EndFunc

Func plot_stop()
	$plot_state = 0
	GUICtrlSetState($bpStart, $GUI_ENABLE)
	GUICtrlSetState($bpStop, $GUI_DISABLE)
	GUICtrlSetData($plot_txt, "Stopped")
EndFunc

;
; PLOT CONTROL FUNCTIONS
; Note: use of $gx and $gy
;

Func config_plot()
	; get dimensions
	Local $y_top = GUICtrlRead($plot1Y)
	Local $x_top = GUICtrlRead($plot1X)
	Local $y_bottom = GUICtrlRead($plot2Y)
	Local $x_bottom = GUICtrlRead($plot2X)
	Local $y_right = GUICtrlRead($plot3Y)
	Local $x_right = GUICtrlRead($plot3X)

	Local $plot_rows = GUICtrlRead($plotRows)
	Local $plot_cols = GUICtrlRead($plotColumns)
	Local $shop_rows = GUICtrlRead($shopRows)
	Local $shop_cols = GUICtrlRead($shopColumns)

	; check rows and cols
	if ($plot_rows = 0 Or $plot_cols = 0 or $shop_rows = 0 or $shop_cols = 0) Then
		MsgBox(0, "error", "row or col cannot be 0")
		return
	EndIf

	; x_scale is half plot width
	; y_scale is half plot height

	; calculate Y scale
	$y_scale = Round((((Abs($y_bottom-$y_top)/$plot_rows)+(Abs($y_bottom-$y_right)/$plot_cols))/2), 0)

	; calculate X scale
	$x_scale = Round((((Abs($x_bottom-$x_top)/$plot_rows)+(Abs($x_right-$x_bottom)/$plot_cols))/2), 0)

	; validate
	if (abs($x_scale - ($y_scale * 2)) <= 2) then
		$x_scale = $y_scale * 2
	else
		MsgBox(0, "error", "scale error - " & $y_scale & ", " & $x_scale)
		return
	EndIf

	GUICtrlSetState($bPlotTest, $GUI_ENABLE)
	GUICtrlSetState($bpStart, $GUI_ENABLE)
EndFunc

Func set_plot($row, $col)
	; Set $gx, $gy to plot coords
	Local $top_y = GUICtrlRead($plot1Y)
	Local $top_x = GUICtrlRead($plot1X)
	Local $i, $j
	Local $x, $y

	; start at centre of plot(0,0)
	$y = $top_y
	$x = $top_x + $x_scale

	$i = 0
	While $i < $row
		$y += $y_scale
		$x += $x_scale
		$i += 1
	Wend

	$j = 0
	While $j < $col
		$y -= $y_scale
		$x += $x_scale
		$j += 1
	Wend

	; set passoff globals
	$gx = $x
	$gy = $y
EndFunc

Func click_plot($row, $col)
	WinActivate($hWnd)
	set_plot($row, $col)
	MouseClick("left", $gx, $gy)
EndFunc

Func plant_corn()
	Local $aPos, $x, $y
	Local $plot_rows = GUICtrlRead($plotRows)
	Local $plot_cols = GUICtrlRead($plotColumns)

	Local $amt = GUICtrlRead($cornAmt)

	; Is there enough to plant the plot
	If ($amt < ($plot_rows * $plot_cols)) Then
		;Msgbox(0, "Error", "Not enough corn to plant")
		Return
	EndIf

	click_plot(0,0)
	sleep(200)

	; check orange handle is available
	; look for orange handle down and left - 60x60

	$x = $gx - 90
	$y = $gy - $y_scale + 50

	$aPos = PixelSearch($x - 100, $y - 100, $x, $y, 0xFCAC00)
	If @error Then
		;msgbox(0, "debug", "Handle Not found")
		return False
	EndIf

	MouseMove($aPos[0], $aPos[1], $speed)
	MouseDown("left")
	sleep(100)
	MouseMove($gx, $gy, $speed)
	scan_plot(-1, 10)
	MouseUp("left")

	return True
EndFunc

Func harvest()
	Local $aPos, $x, $y, $c
	Local $plot_rows = GUICtrlRead($plotRows)
	Local $plot_cols = GUICtrlRead($plotColumns)
	Local $max = GUICtrlRead($cornMax)

	If (($plot_rows * $plot_cols) > $max) Then
		;Msgbox(0, "Error", "Not enough space in silo")
		Return
	EndIf

	click_plot(0,0)
	sleep(200)

	; check that orange arrow is available
	; look for orange handle to the left - 60x60

	$x = $gx - ($x_scale * 2)
	$y = $gy
	$aPos = PixelSearch($x - 100, $y - 100, $x, $y, 0xFCAC00)
	If @error Then
		;msgbox(0, "debug", "Handle Not found")
		return False
	EndIf

	MouseMove($aPos[0], $aPos[1], $speed)
	MouseDown("left")
	sleep(100)
	MouseMove($gx, $gy, $speed)
	scan_plot(2, 9)
	MouseUp("left")
	sleep(1000) ; animation delay

	return True
EndFunc

Func scan_plot($amt, $sspeed)
	; zig zag over the entire plot
	Local $plot_rows = GUICtrlRead($plotRows)
	Local $plot_cols = GUICtrlRead($plotColumns)
	Local $i, $j, $corn
	Local $offset = 1

	; down cols first
	$j = 0
	$i = 0

	Do
		Do
			set_plot($i, $j)
			MouseMove($gx, $gy, $sspeed)

			$corn = GUICtrlRead($cornAmt)
			GUICtrlSetData($cornAmt, $corn + $amt)

			$i += $offset
		Until $i < 0 or $i = $plot_rows
		$i -= $offset ; step back
		$offset = -$offset ; change direction
		$j += 1 ; inc column
	Until $j = $plot_cols
EndFunc

;
; SHOP CONTROL FUNCTIONS
;

Func shop_auto()

	; Scan shop boxes
	; If its sold clear it, if its free fill it
	; Aware of product level by looking for the grey plus button

	Local $box_width = 122
	Local $box_height = 132

	Local $priceX = 37
	Local $priceY = 87
	Local $priceC = 0xFCF8B8

	Local $soldX = 38
	local $soldY = 114

	Local $aPos
	Local $corn
	Local $advert = false

	Local $x, $y, $c, $i, $j

	Local $shop_rows = GUICtrlRead($shopRows)
	Local $shop_cols = GUICtrlRead($shopColumns)

	; If no outstanding corn on sale and none to sell skip shop
	$corn = GUICtrlRead($cornAmt)
	$sales = GUICtrlRead($forSale)
	If ($sales = 0 And $corn < 20) Then
		Return
	EndIf

	If ($shop_rows = 0 Or $shop_cols = 0) Then
		Return
	EndIf

	If ( Not open_shop()) Then
		Return
	EndIf

	; scan across row first
	$i = 0
	$j = 0

	Do
		Do
			$x = $gshopX + ($i * $box_width)
			$y = $gshopY + ($j * $box_height)

			$i += 1

			; is it sold already
			$c = sample($x + $soldX, $y + $soldY)
			If ($c = $whiteC) Then
				; SOLD - click it to clear it
				MouseClick("left", $x + $soldX, $y + $soldY)
				$sale = GUICtrlRead($forSale)
				GUICtrlSetData($forSale, $sale - 10)
				Sleep (200) ; animation
			EndIf

			$corn = GUICtrlRead($cornAmt)

			; is there a price tag?
			$c = sample($x + $priceX, $y + $priceY)
			If ($c <> $priceC) Then
				; box is free
				if ($corn >= 20) Then
					MouseClick("left", $x + $priceX, $y + $priceY) ; open sale panel
					Sleep(100)
					MouseClick("left", 320, 220) ; click silo tab to be sure
					Sleep(100)
					MouseClick("left", 430, 220) ; click first product - always corn
					Sleep(100)
					MouseClick("left", 800, 520) ; click to sell
					GUICtrlSetData($cornAmt, $corn - 10) ; default amount per sale

					$sale = GUICtrlRead($forSale)
					GUICtrlSetData($forSale, $sale + 10)

					Sleep(100)
				EndIf
			Else
				; box has corn in it - check for advert
				If ($advert = False) Then
					$advert = True
					MouseClick("left", $x + $priceX, $y + $priceY) ; open sale panel
					Sleep(100)
					$c = sample(628, 411) ; is there a create advert offer in yellow
					If ($c = 0xF8E44B) Then
						MouseClick("left", 628, 411) ; accept it
					Else
						MouseClick("left", 765, 144) ; Red X close
					EndIf
				EndIf
			EndIf
		Until ($i >= $shop_cols)
		$i = 0
		$j += 1
	Until ($j >= $shop_rows)

	close_shop()

EndFunc

Func open_shop()

	; Open shop menu and pixel search to sync with any variations
	; Note uses passoff global

	Local $x = GUICtrlRead($shopX)
	Local $y = GUICtrlRead($shopY)
	Local $c, $aPos

	WinActivate($hWnd)

	; check activation pixel colour = RED FLOWERS
	MouseMove($x, $y)
	$aPos = PixelSearch($x-10, $y-10, $x+10, $y+10, 0xC86D30, 16)
	If Not @error Then
		MouseClick("left", $aPos[0], $aPos[1])
		sleep (1000) ; refresh delay
		$gshopX = 335 ; offset to left of boxes
		$gshopY = 250 ; offset to top of boxes
		Return True
	Else
		Return False
	EndIf
EndFunc

Func close_shop()
	MouseClick("left", 1071, 99) ; Red X
EndFunc

;
; Utility Functions
;

Func sample($x, $y)
	MouseMove ($x, $y)
	return PixelGetColor ($x, $y, $hWnd)
EndFunc

Func _IsChecked($iControlID)
    Return BitAND(GUICtrlRead($iControlID), $GUI_CHECKED) = $GUI_CHECKED
EndFunc

Func data_lock1()
	$data_lock = 1
EndFunc

Func data_lock2()
	$data_lock = 2
EndFunc

Func data_lock3()
	$data_lock = 3
EndFunc

Func data_lock4()
	$data_lock = 4
EndFunc